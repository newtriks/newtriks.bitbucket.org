var data = [{
  key: 'Male',
  color: '#ff0000',
  values: [{
    "label": "0-5",
    "value": -3
  }, {
    "label": "6-10",
    "value": -8
  }, {
    "label": "11-15",
    "value": -2
  }, {
    "label": "16-20",
    "value": -0
  }, {
    "label": "21-25",
    "value": -9
  }, {
    "label": "26-30",
    "value": -4
  }, {
    "label": "31-35",
    "value": -0
  }, {
    "label": "36-40",
    "value": -8
  }, {
    "label": "41-45",
    "value": -10
  }, {
    "label": "46-50",
    "value": -5
  }, {
    "label": "51-55",
    "value": -6
  }]
}, {
  key: 'Female',
  color: '#0000ff',
  values: [{
    "label": "0-5",
    "value": 0
  }, {
    "label": "6-10",
    "value": 10
  }, {
    "label": "11-15",
    "value": 4
  }, {
    "label": "16-20",
    "value": 9
  }, {
    "label": "21-25",
    "value": 1
  }, {
    "label": "26-30",
    "value": 7
  }, {
    "label": "31-35",
    "value": 5
  }, {
    "label": "36-40",
    "value": 2
  }, {
    "label": "41-45",
    "value": 0
  }, {
    "label": "46-50",
    "value": 5
  }, {
    "label": "51-55",
    "value": 10
  }]
}];

var data2 = [{
  "color": "#ff0000",
  "Male": 24
}, {
  "color": "#0000ff",
  "Female": 23
}, {
  "color": "#00ff80",
  "Unknown": 2
}];

var data3 = function(min, max) {
  return _.times(1000, function(n) {
    return Math.floor(Math.random() * (max - min)) + min;
  });
};
//
// MULTIBAR
//
nv.dev = false;
var chart;
nv.addGraph(function() {
  chart = nv.models.multiBarHorizontalChart()
    .x(function(d) {
      return d.label
    })
    .y(function(d) {
      return d.value
    })
    .margin({
      top: 30,
      right: 30,
      bottom: 50,
      left: 55
    })
    //.showValues(true)
    //.tooltips(false)
    .transitionDuration(250)
    .stacked(true)
    .showControls(false);

  chart.yAxis.tickFormat(function(value) {
    return Math.abs(value);
  }).axisLabel('Patient Count by Gender');
  chart.xAxis.axisLabel('Age').rotateYLabel(false);

  d3.select('#chart1 svg')
    .datum(data)
    .call(chart);

  nv.utils.windowResize(chart.update);

  return chart;
});
//
// PIE
//
var chart2;
nv.addGraph(function() {
  chart2 = nv.models.pieChart()
    .x(function(d) {
      return Object.keys(d)[1]
    })
    .y(function(d) {
      return d[Object.keys(d)[1]]
    })
    .color(function(d) {
      return d.data.color
    })
    .showLabels(true);

  d3.select("#chart2 svg")
    .datum(data2)
    .transition().duration(1200)
    .call(chart2);

  return chart2;
});
//
// HISTOGRAM
//
var formatCount = d3.format(",g");

var margin = {
    top: 10,
    right: 30,
    bottom: 30,
    left: 100
  },
  width = 450 - margin.left - margin.right,
  height = 200 - margin.top - margin.bottom;

var histData = data3(0, 91);

var x = d3.scale.linear()
  .domain([0, _.max(histData)])
  .range([0, width]);

var histogram = d3.layout.histogram()
  //.bins(x.ticks(20)) // Uniformly space...?
  (histData);

var y = d3.scale.linear()
  .domain([0, d3.max(histogram, function(d) {
    return d.y;
  })])
  .range([height, 0]);

var xAxis = d3.svg.axis()
  .scale(x)
  .orient("bottom")
  .tickFormat(function(d, i) {
    return d > 89 ? '90+' : d;
  });

var svg = d3.select("body").append("svg")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var bar = svg.selectAll(".bar")
  .data(histogram)
  .enter().append("g")
  .attr("class", "bar")
  .attr("transform", function(d) {
    return "translate(" + x(d.x) + "," + y(d.y) + ")";
  });

bar.append("rect")
  .attr("x", 1)
  .attr("width", x(histogram[0].dx) - 1)
  .attr("height", function(d) {
    return height - y(d.y);
  });

bar.append("text")
  .attr("dy", ".75em")
  .attr("y", 6)
  .attr("x", x(histogram[0].dx) / 2)
  .attr("text-anchor", "middle")
  .text(function(d) {
    return formatCount(d.y);
  });

svg.append("text")
  .attr("class", "x label")
  .attr("text-anchor", "end")
  .attr("x", width / 2)
  .attr("y", height + 50)
  .text("AGE");

svg.append("g")
  .attr("class", "x axis")
  .attr("transform", "translate(0," + height + ")")
  .call(xAxis);
